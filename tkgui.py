from Tkinter import *
import tkMessageBox
#import matplotlib.pyplot as plt
import os
import time
import datetime
#import sympy
from random import uniform
from math import trunc
import csv

#------------------#
#-- THINGS TO DO --#
#------------------#
# 1- Rewrite podStatus as dictionary (1= init, 2= ready, 3= accel, etc)
# 2- Rewrite GUI into Classes
# 3- Write simulated Flight code
# 4- Put all debug controls into new window

#------------------#
#-- OVERVIEW     --#
#------------------#
#
# Anything marked DEBUG should be removed or commented out for release.
# 

#-- Open Windows --#
root = Tk()


#-- DEBUG --#

dtrack = IntVar()
dtrack.set(1)
daccel = DoubleVar()
daccel = 15
dtrackE = IntVar()
#dtrack2 = IntVar()
daccelE = DoubleVar()
i = IntVar()
i.set(0)

def debug():
    debug = Tk()
    debug.minsize(width=150, height=50)
   
    dtrackM = Message(debug, text="Track Length: ", width=100).grid(row=0,column=0)
    dtrackE = Entry(debug, bd=2, width=6)
    dtrackE.insert(0, trackLength.get())
    dtrackE.grid(row=0,column=1)
       
    daccelM = Message(debug, text="Accel delay: ", width=120).grid(row=1,column=0)
    daccelE = Entry(debug, bd=2, width=6)
    daccelE.insert(0, accelTime.get())
    daccelE.grid(row=1,column=1)

    pushButton = Button(debug, text="Push Updates", command=lambda : debugPush(debug, dtrackE, daccelE))
    pushButton.grid(row=2,column=0,columnspan=2)

def readPressure():
    data = []
    pressure = []
    with open('vacTest1.dat', 'r') as f:
        data = csv.reader(f, delimiter=',')
        for i in data:
            pressure.append((i[7]).strip(')'))
        return(pressure)

def debugPush(window, dtrackE, daccelE):
    trackLength.set(int(dtrackE.get()))
    accelTime.set(daccelE.get())
    window.destroy()

#-- Init --#
def podHealth():
    pod = Tk()
    pod.title("Pod Health")
    statusLabel = Label(pod, text="Pod is healthy!").grid(row=0, column=0)
    print("Pod is healthy.")

def hlOptions():
    print("Change Options.")

def quitFunc():
    if tkMessageBox.askyesno("Quit","Are you sure?"): quit()

def elapsedTimeFunc():
    t = time.time() - start_time.get()
    minutes = trunc(t/60)
    elapsedTime.set(str(minutes)+":"+str(round((t-(minutes*60)),2)))

def launchFunc():
    startFlight.set(time.time())
    podStatus.set("accel")
    flightTime_B.config(state=DISABLED)

def flightTimeFunc():
    if startFlight.get() > 0:
        t = time.time() - startFlight.get()
        minutes = trunc(t/60)
        flightTime.set(str(minutes)+":"+str(round((t-(minutes*60)),2)))
        if t > accelTime.get():
            podStatus.set('coast')

def functionalTest():
    funcState = StringVar()
    funcState.set("INIT")
    
    sensorBrakePos.set(3.14)
    sensorWheelRPM.set(0)
    if (sensorBrakePos.get()==3.14) and (batteryVoltage.get() > 11.5) \
    and (batteryVoltage.get() < 14) and (sensorWheelRPM.get() == 0):
        funcState.set("PASS")
        podStatus.set("ready")
        tkMessageBox.showinfo("","Functional Test Pass")
     
        
def changeStatus(status):
    brakeInhibit.set(podStatus2[status][0])
    brakeL.config(bg=podStatus2[status][1], fg=podStatus2[status][2])
    status_B.config(state=podStatus2[status][3])

#-- Dynamic Updating --#
def poll():

    batteryVoltage.set(round(uniform(11.0,15.0),2)) # Debug random volt generator
    if batteryVoltage.get() < 11.5: tl_battL.config(bg="red")
    elif batteryVoltage.get() > 14: tl_battL.config(bg="red")
    else: tl_battL.config(bg="green")

    sensor_IMUx.set(round(uniform(-2,2),2)) # Debug random float
    sensor_IMUy.set(round(uniform(-2,2),2)) # Debug random float
    sensor_IMUz.set(round(uniform(-2,2),2)) # Debug random float

    sensorBrakePos.set(round(uniform(0,4),1)) # Debug random float
    
    pressure = []
    for x in readPressure():
        pressure.append(x)
    sensorPres.set(pressure[i.get()])
    i.set(i.get()+1)
    
    sensorWheelRPM.set(round(uniform(0,3000),0)) # Debug random int

    #cpu_raw = os.popen('vcgencmd measure_temp').readline()
    #cpuTemp.set(cpu_raw.replace("temp=","").replace("'C\n",""))
    cpuTemp.set(45)
    if cpuTemp.get() > 60: cpuTemp_L.config(bg="red", fg="white")
    else: cpuTemp_L.config(bg="green")

    # if podStatus.get() == "init":
    #     brakeInhibit.set("OFF")
    #     brakeL.config(bg="green", fg="white")
        
    # elif podStatus.get() == "ready":
    #     brakeInhibit.set("ON")
    #     brakeL.config(bg="red", fg="white")
        
    # elif podStatus.get() == "accel":
    #     brakeInhibit.set("ON")
    #     brakeL.config(bg="red", fg="white")
    #     status_B.config(state=DISABLED)
    # elif podStatus.get() == "coast":
    #     brakeInhibit.set("OFF")
    #     brakeL.config(bg="green", fg="white")
    #     status_B.config(state=DISABLED)
    # elif podStatus.get() == "brake":
    #     brakeInhibit.set("OFF")
    #     brakeL.config(bg="green", fg="white")
    #     status_B.config(state=DISABLED)
    
    changeStatus(podStatus.get())
    elapsedTimeFunc()
    flightTimeFunc()
    root.after(250, poll)

#-- Var Init --#
trackLength = IntVar()
trackLength.set(4500)
accelTime = DoubleVar()
accelTime.set(15)
batteryVoltage = DoubleVar()
batteryVoltage.set(14.5)
battWarn = StringVar()

sensor_IMUx = DoubleVar()
sensor_IMUy = DoubleVar()
sensor_IMUz = DoubleVar()

sensorBrakePos = DoubleVar()

sensorPres = IntVar()

sensorWheelRPM = IntVar()

cpuTemp = DoubleVar()

start_time = DoubleVar()
start_time.set(time.time())
start_time2 = datetime.datetime.now()

brakeInhibit = StringVar()
brakeInhibit.set("boot")

elapsedTime = StringVar()
flightTime = StringVar()
startFlight = DoubleVar()


#-- Status Bar --#
podStatus = StringVar()
podStatus.set("init")
statusBarL = Label(root, textvariable=podStatus, relief=SUNKEN, bd=1, anchor=W)
statusBarL.grid(row=3, columnspan=4, sticky=W+E)

#podStatus2 = (brakeInhibit_L, brakeInhibit.config(bg), brakeInhibit.config(fg),
#             status_B.config(state))
podStatus2 = {"init":("OFF","green","white",NORMAL),
                "ready":("ON", "red", "white", NORMAL),
                "accel":("ON", "red", "white", DISABLED),
                "coast":("OFF", "green", "white", DISABLED),
                "brake":("OFF", "green", "white", DISABLED)}

#-- Menu --#
menu = Menu(root)
root.config(menu=menu)
root.title("Hyperlynx II")

filemenu = Menu(menu)
menu.add_cascade(label="Options", menu=filemenu)
filemenu.add_command(label="Pod Health Check", command=podHealth)
filemenu.add_command(label="Debug Options", command=lambda:debug())
filemenu.add_command(label="Quit", command=quitFunc)

#---------#
#-- GUI --#
#---------#

 # Pod Health Frame
phFrame = Frame(root, bd=1, relief=SUNKEN, bg="white")
phFrame.grid(row=0, column=0, columnspan=2, sticky=W+E)

ph_1 = Message(phFrame, text="Pod Health", font=("Courier, 14"), anchor=W, width=120, bg="white").grid(row=0, column=0, sticky=W+E, columnspan=2)

tl_battM = Message(phFrame, text="Batt [V]", width=120, bg="white").grid(row=1,column=0, sticky=W)
tl_battL = Label(phFrame, textvariable=batteryVoltage, width=5, fg="white")
tl_battL.grid(row=1,column=1, sticky=E)

cpuTemp_M = Message(phFrame, text="CPU Temp [C]", width=120, bg="white").grid(row=2,column=0, sticky=W)
cpuTemp_L = Label(phFrame, textvariable=cpuTemp, width=5, fg="white")
cpuTemp_L.grid(row=2,column=1, sticky=E)

ph_2 = Message(phFrame, text="Sensor Data", font=("Courier, 14"), anchor=W, width=120, bg="white")
ph_2.grid(row=3, column=0, sticky=W, pady=10)

sensorPres_M = Message(phFrame, text="Air Pressure [mb]", width=120, bg="white").grid(row=4,column=0, sticky=W)
sensorPres_L = Label(phFrame, textvariable=sensorPres, width=7)
sensorPres_L.grid(row=4,column=1, sticky=E)

sensorIMUx_M = Message(phFrame, text="Gyro X", width=120, bg="white").grid(row=5,column=0, sticky=W)
sensorIMUx_L = Label(phFrame, textvariable=sensor_IMUx, width=5)
sensorIMUx_L.grid(row=5,column=1, sticky=E)

sensorIMUy_M = Message(phFrame, text="Gyro Y", width=120, bg="white").grid(row=6,column=0, sticky=W)
sensorIMUy_L = Label(phFrame, textvariable=sensor_IMUy, width=5)
sensorIMUy_L.grid(row=6,column=1, sticky=E)

sensorIMUz_M = Message(phFrame, text="Gyro Z", width=120, bg="white").grid(row=7,column=0, sticky=W)
sensorIMUz_L = Label(phFrame, textvariable=sensor_IMUz, width=5)
sensorIMUz_L.grid(row=7,column=1, sticky=E)

sensorWheelRPM_M = Message(phFrame, text="Wheel RPM", width=120, bg="white").grid(row=8, column=0, sticky=W)
sensorWheelRPM_L = Label(phFrame, textvariable=sensorWheelRPM, width=6)
sensorWheelRPM_L.grid(row=8, column=1, sticky=E)

sensorBrakePos_M = Message(phFrame, text="Brake Position", width=120, bg="white").grid(row=9, column=0, sticky=W)
sensorBrakePos_L = Label(phFrame, textvariable=sensorBrakePos, width=5)
sensorBrakePos_L.grid(row=9, column=1, sticky=E)
sensorBrakePos_S = Scale(phFrame, from_=0, to=4, orient=HORIZONTAL, state=DISABLED, variable=sensorBrakePos, resolution=0.1, bg="white", sliderlength=10, length=150)
sensorBrakePos_S.grid(row=10, column=0, columnspan=2)

elapsedTime_M = Message(phFrame, text="GUI Time", width=120, bg="white").grid(row=11, column=0, sticky=W, pady=10)
elapsedTime_L = Label(phFrame, textvariable=elapsedTime, width=10)
elapsedTime_L.grid(row=11, column=1, sticky=E)


 # Options Frame
optFrame = Frame(root, bd=1, relief=SUNKEN)
optFrame.grid(row=1, column=0, sticky=W+E)

title1 = Message(optFrame, text="Options", font=("Courier, 12"), anchor=W, width=120, bg="white").grid(sticky=W+E, columnspan=2)

trackM = Message(optFrame, text="Track Length [ft]", width=120, bg="white").grid(row=1,column=0, sticky=W)
trackL = Label(optFrame, textvariable=trackLength, width=5).grid(row=1,column=1, sticky=E)

accelM = Message(optFrame, text="Accel Delay [s]", width=120, bg="white").grid(row=2,column=0, sticky=W)
accelL = Label(optFrame, textvariable=accelTime, width=5).grid(row=2,column=1, sticky=E)

 # Top Right Frame
posFrame = Frame(root, bd=1) # Columns 2 and 3 of root
posFrame.grid(row=0, column=2, sticky=N+W+E, columnspan=2)

title2 = Message(posFrame, text="Status: ", font=("Courier, 14"), width=120).grid(row=0, sticky=W)
status_L = Label(posFrame, textvariable=podStatus, width=10)
status_L.grid(row=0, column=1, sticky=W)
status_B = Button(posFrame, text="Func Test", command=functionalTest, width=10)
status_B.grid(row=0, column=2, sticky=E)

  

flightTime_M = Message(posFrame, text="Flight Time: ", width=120).grid(row=1, sticky=W)
flightTime_L = Label(posFrame, textvariable=flightTime, width=10)
flightTime_L.grid(row=1, column=1, sticky=W)
flightTime_B = Button(posFrame, text="debug launch", command=launchFunc, width=10)
flightTime_B.grid(row=1, column=2, sticky=W)

brakeM = Message(posFrame, text="Brake Inhibition", width=120).grid(row=2,column=0, sticky=W)
brakeL = Label(posFrame, textvariable=brakeInhibit, width=10)
brakeL.grid(row=2,column=1, sticky=E)



poll()
root.mainloop()
